// Initialize and add the map
function initMap() {
    // The geo coordinates of Hyderabad are being provided.
    const geo_coordinates = { lat: 17.387140, lng: 78.491684 };
    
    // The map, centered at Hyderabad
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 10,
      center: geo_coordinates,
    });
    
    // The marker, positioned at Hyderabad, India
    const marker = new google.maps.Marker({
      position: geo_coordinates,
      map: map,
    });
  }